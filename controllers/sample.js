// Sample API
// @method GET
// @route /sample
exports.getSample = (req, res, next) => {
  const message = 'sample message of cicd pipeline';
  res.status(200).json({message});
}