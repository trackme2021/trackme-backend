const express = require("express");
const dotenv = require("dotenv");
const morgan = require("morgan");
const colors = require("colors");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const connectDB = require("./config/db");

// Load environment variables
dotenv.config({ path: "./config/config.env" });


// Route files
const sample = require("./routes/sample");

// Setup app
const app = express();

// Connect to database
connectDB();

app.use(express.json());

// Add cors for overcome from cors policy
app.use(cors());

// Cookie Parser
app.use(cookieParser());

// Dev logging middleware
process.env.NODE_ENV === "development" ? app.use(morgan("dev")) : null;

//Mount routers
app.use("/api/v1/sample", sample);

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(
    `Server Running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold
  )
);

// Handle Unhandled Rejection
process.on("unhandledRejection", (err, promise) => {
  console.log(`Unhandled Rejection : ${err.message}`.red);
  // Close Server
  server.close(() => process.exit(1));
});
